# ebdmn-admin-vue-vite-element

### 介绍

### 软件架构
软件架构说明
基于vue^3.1.4 + element-plus ^1.2.0-beta.6开发

### 安装教程

# 克隆项目
```
git clone
```

##### 进入项目目录
```
cd ebdmn-admin-vue-vite-element
```

##### 安装依赖
```
npm install （推荐使用yarn）
```

##### 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
```
npm install --registry=https://registry.npm.taobao.org
```
##### 启动服务
```
npm run dev (yarn run dev)
```

浏览器访问 http://localhost:3000

#### 使用说明

1.  样式前缀规范 ebdmn-
2.  xxxx
3.  xxxx

### 目录结构说明

.
├── README.en.md                                            //英文说明文档
├── README.md                                               //中文说明文档
├── index.html                                              //进入系统之前的loading页面
├── index2.html                                             // 备份
├── package-lock.json                                       //npm 配置文件（锁定，自动生成的）
├── package.json                                            //npm
├── public                                                  // 公共资源信息，例如logo、背景图
│         ├── favicon.ico
│         └── img
│             └── tabBg.png
├── src                                                     // 源代码路径
│         ├── App.vue                                       // 主组件，页面入口文件
│         ├── api                                           //  api请求封装
│         │         ├── router.js                           // 路由相关的封装
│         │         └── user.js                             // 用户相关的请求封装
│         ├── assets                                        // 资源文件
│         │         ├── icon                                // 图标
│         │         │         ├── iconfont.css
│         │         │         ├── iconfont.eot
│         │         │         ├── iconfont.js
│         │         │         ├── iconfont.json
│         │         │         ├── iconfont.svg
│         │         │         ├── iconfont.ttf
│         │         │         ├── iconfont.woff
│         │         │         └── iconfont.woff2
│         │         ├── login_images                        // 登录图片
│         │         │         ├── login_background.png
│         │         │         └── login_form.png
│         │         └── logo.png                            // logo图片
│         ├── components                                    // 平台相关组件
│         │         ├── EbdmnHamburger                      // 收缩按钮组件（展开关闭菜单栏）
│         │         │         └── index.vue
│         │         ├── EbdmnIcon                           // 自定义图标
│         │         │         └── index.vue
│         │         ├── EbdmnLabel                          // 自定义label组件，详见组件示例
│         │         │         ├── index.vue
│         │         │         └── layout.vue
│         │         ├── EbdmnLabelCheckbox                  // 自定义CheckBox label组件
│         │         │         └── index.js
│         │         ├── EbdmnLabelRadio                     // 自定义Radio label组件
│         │         │         └── index.js
│         │         ├── EbdmnLayout                         // 自定义layout布局
│         │         │         └── index.vue
│         │         ├── EbdmnToolbar                        // 自定义toolbar工具栏
│         │         │         ├── index.js
│         │         │         ├── layout.vue
│         │         │         └── panel.vue
│         │         └── layout                              // 主界面布局组件
│         │             ├── EbdmnAside.vue                  // 右侧logo及菜单栏组件
│         │             ├── EbdmnAvatarComponent.vue        // 头像组件
│         │             ├── EbdmnContentComponent.vue       // 主界面内容组件
│         │             ├── EbdmnLogoComponent.vue          // logo组件
│         │             ├── EbdmnMenuComponent.vue          // 菜单栏组件
│         │             ├── EbdmnMenuItem.vue               // 菜单项组件
│         │             ├── EbdmnMenuTabsComponent.vue      // 标签页组件
│         │             ├── EbdmnMenuTabsComponent_old.vue
│         │             ├── EbdmnSubmenu.vue                // 子菜单组件
│         │             └── TagsView                        // 另一种方式实现标签页
│         │                 ├── ScrollPane.vue
│         │                 └── index.vue
│         ├── config                                        // 平台配置文件
│         │         └── index.js
│         ├── ebdmn                                         // 自定义插件
│         │         ├── index.js
│         │         └── plugins                             // 插件
│         │             ├── banner.js                       // 打包的时添加banner
│         │             ├── html.js                         // 浏览器控制台打印版权信息
│         │             └── permissions.js                  // 权限相关
│         ├── layout                                        // 主界面布局
│         │         └── index.vue
│         ├── main.js                                       // 是初始化vue实例并使用需要的插件
│         ├── plugins                                       // 插件
│         │         ├── download.js                         // 下载文件
│         │         └── index.js
│         ├── router                                        // 路由相关
│         │         └── index.js
│         ├── store                                         // 缓存相关
│         │         ├── index.js
│         │         └── modules                             // 根据模块区分
│         │             ├── routes.js                       // 路由相关缓存
│         │             ├── settings.js                     // 设置相关缓存
│         │             ├── tagsBar.js                      // 标签页相关缓存
│         │             ├── tagsView.js
│         │             └── user.js                         // 用户相关缓存
│         ├── styles
│         │         └── ebdmn-styles                        // 整体样式
│         ├── utils                                         // 工具类
│         │         ├── accessToken.js                      // token工具类
│         │         ├── ebdmn.js                            // 平台自定义工具类
│         │         ├── hasRole.js                          // 角色相关
│         │         ├── pageTitle.js                        // 设置标题
│         │         ├── request.js                          // 请求封装
│         │         ├── routes.js                           // 路由封装
│         │         └── validate.js                         // 校验相关
│         └── views                                         // 页面
│             ├── basic                                     // 平台基础功能
│             │         ├── api                             // api请求封装
│             │         │         ├── databook.js
│             │         │         ├── modules.js
│             │         │         ├── permission.js
│             │         │         └── user.js
│             │         ├── components                      // 模块自定义组件
│             │         │         └── DataBook.vue
│             │         └── views                           // 模块页面
│             │             ├── databook                    // 数据字典
│             │             │         ├── List.vue          // 数据字典列表
│             │             │         └── index.vue         // 数据字典入口index
│             │             ├── modules                     // 菜单页面
│             │             │         ├── Form.vue          // 菜单表单
│             │             │         ├── List.vue          // 菜单列表
│             │             │         └── index.vue         // 菜单入口index
│             │             ├── permission
│             │             │         ├── List.vue
│             │             │         └── index.vue
│             │             └── user
│             │                 ├── List.vue
│             │                 └── index.vue
│             ├── demo                                      // 示例
│             │         ├── api
│             │         └── views
│             │             ├── EbdmnLabelDemo
│             │             │         └── index.vue
│             │             └── ListAndFormDemo
│             │                 ├── Form.vue
│             │                 ├── List.vue
│             │                 └── index.vue
│             ├── error                                     // 错误页面
│             │         ├── 403.vue
│             │         └── 404.vue
│             ├── index                                     
│             │         └── index.vue
│             ├── login                                     // 登录界面
│             │         └── index.vue
│             ├── mobile                                    // 移动端
│             │         ├── api
│             │         │         ├── banner.js
│             │         │         └── message.js
│             │         └── views
│             │             ├── banner
│             │             │         ├── form.vue
│             │             │         ├── index.vue
│             │             │         └── list.vue
│             │             └── message
│             │                 ├── form.vue
│             │                 ├── index.vue
│             │                 └── list.vue
│             └── tool                                      // 系统提供相关工具
│                 ├── api
│                 │         └── gen.js
│                 ├── components
│                 ├── utils
│                 │         └── utils.js
│                 └── views
│                     └── gen
│                         ├── basicInfoForm.vue
│                         ├── editTable.vue
│                         ├── genInfoForm.vue
│                         ├── importTable.vue
│                         ├── index.vue
│                         └── list.vue
├── vite.config.js                                          // vite配置文件
└── yarn.lock

### 依赖说明
| 序号  | 依赖项  | 依赖说明                       | 备注                   |
|-----|------|----------------------------|----------------------|
| 1   | @element-plus/icons-vue | Element Plus图标             |                      |
| 2   | @highlightjs/vue-plugin | 代码高亮插件                     | 用于代码展示               |
| 3   | axios | 基于 promise 的 HTTP 库        | 用于请求后端               |
| 4   | dayjs | 一个轻量的处理时间和日期的 JavaScript 库 |                      |
| 5   | element-plus |                            |                      |
| 6   | file-saver | 文件处理                       |                      |
| 7   | highlight.js | 代码高亮                       |                      |
| 8   | js-cookie |                            |                      |
| 9   | less | less                       |                      |
| 10  | less-loader | less-loader                |                      |
| 11  | remixicon | 开源图标库                      |                      |
| 12  | vue-clipboard2 | 复制插件                       |                      |
| 13  | vue-router | 路由                         |                      |
| 14  | vuex | 状态                         |                      |
| 15  | vue3-count-to | 滚动数字的效果                    | 数据可视化大屏开发的过程当中,须要实现一种滚动数字的效果 |
| 16  | vite-plugin-banner | banner                     |                      |
| 16  | vite-plugin-html | 控制台输入版权信息                  | 装逼用                  |
| 16  | unplugin-vue-components | 自动导入                       |                      |
| 16  | unplugin-auto-import | 自动导入                  |                      |

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
