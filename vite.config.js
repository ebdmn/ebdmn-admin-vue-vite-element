import { defineConfig, loadEnv } from 'vite'
import createVitePlugins from './src/ebdmn/index'
const path = require('path')
// 文件的读写
const fs = require('fs')
// https://vitejs.dev/config/
// mode server
// command 环境 development
export default ({ mode, command }) => {
  // 获取环境变量
  const env = loadEnv(mode, process.cwd())

  // 全局 scss 资源
  const scssResources = []

  // 加载文件
  fs.readdirSync('src/styles/ebdmn-styles/theme').map(dirname => {
    if (fs.statSync(`src/styles/ebdmn-styles/theme/${dirname}`).isFile()) {
      scssResources.push(`@use "src/styles/ebdmn-styles/theme/${dirname}" as *;`)
    }
  })
  return defineConfig({
    base: '/',
    // 开发服务器选项 https://cn.vitejs.dev/config/#server-options
    server: {
      open: true,
      port: 3000,
      proxy: {
        '/auth': {
          target: 'https://www.ebdmn.com',
          changeOrigin: true
        },
        '/basic': {
          target: 'https://www.ebdmn.com',
          changeOrigin: true
        },
        '/code': {
          target: 'https://www.ebdmn.com',
          changeOrigin: true
        },
        '/mobile': {
          target: 'https://www.ebdmn.com',
          changeOrigin: true
        },
        '/activiti': {
          target: 'https://www.ebdmn.com',
          changeOrigin: true
        },
        '/gateway': {
          target: 'https://www.ebdmn.com',
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/gateway/, '')
        },
        '/message': {
          target: 'https://www.ebdmn.com',
          changeOrigin: true
          // rewrite: (path) => path.replace(/^\/message/, '')
        },
        '/fileservice': {
          target: 'https://www.ebdmn.com',
          changeOrigin: true
        },
        '/websocket': {
          target: 'ws://www.ebdmn.com',
          changeOrigin: true
          // rewrite: (path) => path.replace(/^\/websocket/, '')
        },
        '/job': {
          target: 'http://127.0.0.1:8004',
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/job/, '')
        }
        // '/dev-api': {
        //   target: 'http://127.0.0.1:8080',
        //   changeOrigin: command === 'serve' && env.VITE_OPEN_PROXY == 'true',
        //   rewrite: path => path.replace(/\/dev-api/, '')
        // }
      }
    },
    resolve: {
      alias: {
        '@': path.resolve('src'),
        '~@': path.resolve('src'),
        '@config': path.resolve('src/config'),
        '@components': path.resolve('src/components'),
        '@layout': path.resolve('src/layout'),
        '@store': path.resolve('src/store'),
        '@styles': path.resolve('src/styles'),
        '@router': path.resolve('src/router'),
        '@assets': path.resolve('src/assets'),
        '@utils': path.resolve('src/utils'),
        '@basic': path.resolve('src/views/basic'),
        '@activiti': path.resolve('src/views/activiti'),
        '@demo': path.resolve('src/views/demo'),
        '@job': path.resolve('src/views/job'),
        '*': path.resolve('')
      }
    },
    plugins: createVitePlugins(env, command === 'build'),
    // vite2.x
    build: {
      minify: false
    },
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: scssResources.join('')
        },
        less: {
          javascriptEnabled: true
        }
      }
    }
  })
}

