import { createRouter, createWebHashHistory } from 'vue-router'
import Layout from '@/layout/index.vue'

export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index.vue'),
    hidden: true
  },
  {
    path: '/403',
    name: '403',
    component: () => import('@/views/error/403.vue'),
    hidden: true
  },
  {
    path: '/404',
    name: '404',
    component: () => import('@/views/error/404.vue'),
    hidden: true
  }
]
export const asyncRoutes = [
  {
    path: '/',
    component: Layout,
    redirect: '/index',
    meta: {
      title: '工作台',
      icon: 'desktop',
      affix: true
    },
    children: [
      {
        path: '/index',
        name: 'Index',
        component: () => import('@/views/index/index.vue'),
        meta: {
          title: '工作台',
          icon: 'desktop',
          affix: true
        }
      }
    ]
  },
  {
    path: '/demo',
    component: Layout,
    redirect: '/demo/EbdmnLabelDemo',
    meta: {
      title: '测试',
      icon: 'demo-tube-line'
    },
    children: [
    ]
  },
  // {
  //   path: '/components',
  //   component: Layout,
  //   redirect: '/components/EbdmnLabelDemo',
  //   meta: {
  //     title: '组件',
  //     icon: 'demo-tube-line',
  //   },
  //   children: [
  //     {
  //       path: 'EbdmnLabelDemo',
  //       name: 'Test1',
  //       component: () => import('@/views/demo/views/EbdmnLabelDemo/index.vue'),
  //       meta: {
  //         title: 'EbdmnLabelDemo',
  //         icon: 'demo-tube-line',
  //       },
  //     },
  //     {
  //       path: 'test2',
  //       name: 'Test2',
  //       component: () => import('@/views/demo/views/test2/index.vue'),
  //       meta: {
  //         title: 'test2',
  //         icon: 'demo-tube-line',
  //       },
  //     },
  //     {
  //       path: 'test3',
  //       name: 'Test3',
  //       component: () => import('@/views/demo/views/test3/index.vue'),
  //       meta: {
  //         title: 'test3',
  //         icon: 'demo-tube-line',
  //       },
  //     },
  //     {
  //       path: 'test4',
  //       name: 'Test4',
  //       component: () => import('@/views/demo/views/test4/index.vue'),
  //       meta: {
  //         title: 'test4',
  //         icon: 'demo-tube-line',
  //       },
  //     },
  //   ],
  // },
  // {
  //   path: '/error',
  //   name: 'Error',
  //   component: Layout,
  //   redirect: '/error/403',
  //   meta: {
  //     title: '错误页',
  //     icon: 'error-warning-line',
  //   },
  //   children: [
  //     {
  //       path: '403',
  //       name: 'Error403',
  //       component: () => import('@/views/error/403.vue'),
  //       meta: {
  //         title: '403',
  //         icon: 'error-warning-line',
  //       },
  //     },
  //     {
  //       path: '404',
  //       name: 'Error404',
  //       component: () => import('@/views/error/404.vue'),
  //       meta: {
  //         title: '404',
  //         icon: 'error-warning-line',
  //       },
  //     },
  //   ],
  // },
  {
    path: '/*',
    redirect: '/404',
    hidden: true
  }
]
const router = createRouter({
  history: createWebHashHistory(),
  routes: constantRoutes
})

export default router
