import request from '@/utils/request'

export async function getList(data) {
  return request({
    url: '/activiti/task/getTaskList',
    method: 'post',
    data: data
  })
}
// TODO 切换 用户为后台 信息
export async function startProcess(data, params) {
  return request({
    url: '/activiti/process-instance/startProcess',
    method: 'post',
    params: params,
    data: data
  })
}
// TODO 切换 用户为后台 信息
export async function taskComplete(data, params) {
  return request({
    url: '/activiti/task/complete',
    method: 'post',
    params: params,
    data: data
  })
}
// TODO 切换 用户为后台 信息
export async function closeProcess(instanceId, params) {
  return request({
    url: '/activiti/process-instance/closeProcess/' + instanceId,
    method: 'post',
    params: params
  })
}
export async function history(data) {
  return request({
    url: '/activiti/history/' + data.instanceId,
    method: 'get'
  })
}
export async function historiesByUniqueCode(uniqueCode) {
  return request({
    url: '/activiti/historiesByUniqueCode/' + uniqueCode,
    method: 'get'
  })
}
export function imageByte(data) {
  return request({
    url: '/activiti/imageByte/' + data.instanceId,
    method: 'get',
    responseType: 'arraybuffer'
  })
}
