# 自定义组件说明

### 介绍

### SubmitApproval
审批发起组件，三个参数， 审批相关参数、成功回调方法、关闭回调方法
#### 示例
```js
SubmitApproval(
          {
            processDefineKey: "test", // 流程编码
            businessKey: "3",  // 业务数据ID，后期关联业务数据的
            businessPath: 'views/activiti/views/task/Form.vue', // 表单审批的vue文件，src下的内容
            businessDesc: 'XXX业务审批', // 表单审批的标题
            businessModule: 'EbdmnActivitiTaskIndex', // 审批模块标识
            processVars: {   // 流程比变量
              vars1: 'value1',
              vars2: 'value2',
              vars3: 'value3',
              vars4: 'value4',
              vars5: 'value5',
              vars6: 'value6',
            }
          },
          this.callBack, // 确认回调方法
          this.close  // 关闭回调方法
      )
```

### FormApprovalLayout
表单审批功能, 参数1个 审批的相关
#### 示例
```js
formApprovalLayout({
        instanceId: row.instanceId,
        taskId: row.taskId,
        taskVars: row.processVars
      })
```

### TaskApproval
dialog审批功能 
#### 示例
```js
TaskApproval(
          {
            instanceId: row.instanceId,
            taskId: row.taskId
          }
      )
```

### WorkFlowDetail
流程历史组件
```js

```
