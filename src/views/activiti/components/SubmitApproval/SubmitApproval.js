
// import Vue from 'vue';
// import SubmitApprovalView from '@activiti/components/SubmitApproval/index.vue'
// const ApprovalConstructor = Vue.extend(SubmitApprovalView);
import { startProcess } from '@activiti/api/activiti.js'
// let instance;
// let backFuns
const SubmitApproval = function(options, backFun, closeBackFun, params) {
  startProcess(options, params).then(response => {
    if (response.code === '20000') {
      backFun(response.dataSets)
    } else {
      this.$message.error(response.message)
      closeBackFun()
    }
  }
  )
  // if (Vue.prototype.$isServer) return;
  // options = options || {};
  // backFuns = backFun
  // instance = new ApprovalConstructor({
  //   data: options,
  //   methods: {
  //     backFun: backFun,
  //     closeBackFun: closeBackFun
  //   }
  // });
  // instance.enableApproval(backFun)
  // instance.$mount();
}
// SubmitApproval.selectUsers = function(options) {
//   backFuns(options)
// }
export default SubmitApproval
