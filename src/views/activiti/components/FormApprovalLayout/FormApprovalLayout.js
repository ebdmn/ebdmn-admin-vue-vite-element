import { createApp, defineAsyncComponent } from 'vue'
import component from '@activiti/components/FormApprovalLayout/index.vue'

const modules = import.meta.glob('../../../../views/**/*.vue')

import { ElButton } from 'element-plus'
import { ElDialog } from 'element-plus'
import { ElRow } from 'element-plus'
import { ElCol } from 'element-plus'
import { ElForm } from 'element-plus'
import { ElFormItem } from 'element-plus'
import { ElRadioGroup } from 'element-plus'
import { ElRadio } from 'element-plus'
import { ElInput } from 'element-plus'
import { ElOption } from 'element-plus'
import { ElSelect } from 'element-plus'
import { ElTable } from 'element-plus'
import { ElTableColumn } from 'element-plus'
import { ElDrawer } from 'element-plus'

const formApprovalLayout = function(options, backFun, closeBackFun) {
    let AsyncComp = {}
    // 是否配置表单展示vue，如果没配置显示空白vue
    if (options.taskVars.businessPath) {
        AsyncComp = defineAsyncComponent(
            // importLocale(options.taskVars.businessPath)
            modules['../../../../' + options.taskVars.businessPath]
        )
    } else {
        defineAsyncComponent(
            // import('@/views/activiti/views/emptyPage/index.vue')
            modules['../../../../views/activiti/views/emptyPage/index.vue']
        )
    }
    return new Promise((resolve) => {
        // 实例化组件，createApp第二个参数是props
        const instance = createApp(component, {
            instanceId: options.instanceId,
            taskId: options.taskId,
            businessKey: options.taskVars.businessKey,
            taskVars: options.taskVars,
            showTitle: true,
            title:  options.taskVars.businessDesc,
            backFun: (params) => {
                if (typeof backFun === 'function') {
                    backFun(params)
                }
                unmount()
                resolve()
            },
            closeBackFun: () => {
                if (typeof closeBackFun === 'function') {
                    closeBackFun()
                }
                unmount()
                resolve()
            }
        })

        // 新挂载容器使用的组件
        instance.use(ElButton)
        instance.use(ElDialog)
        instance.use(ElRow)
        instance.use(ElCol)
        instance.use(ElForm)
        instance.use(ElFormItem)
        instance.use(ElRadioGroup)
        instance.use(ElRadio)
        instance.use(ElInput)
        instance.use(ElOption)
        instance.use(ElSelect)
        instance.use(ElTable)
        instance.use(ElTableColumn)
        instance.use(ElDrawer)

        instance.component('async-component', AsyncComp)

        // 创建一个挂载容器
        const parentNode = document.createElement('div')
        document.body.appendChild(parentNode)

        // 卸载组件
        const unmount = () => {
            instance.unmount()
            // this.$destroy();
            if (document.body.contains(parentNode)) {
                document.body.removeChild(parentNode);
            }
            // document.body.removeChild(parentNode)
        }

        // 挂载组件
        instance.mount(parentNode)
    })
}
function importLocale(locale) {
    return import( /* @vite-ignore */ `/src/${locale}`);
}
export default formApprovalLayout;