import { createApp } from 'vue'
import component from '@activiti/components/TaskApproval/index.vue'

import { ElButton } from 'element-plus'
import { ElDialog } from 'element-plus'
import { ElRow } from 'element-plus'
import { ElCol } from 'element-plus'
import { ElForm } from 'element-plus'
import { ElFormItem } from 'element-plus'
import { ElRadioGroup } from 'element-plus'
import { ElRadio } from 'element-plus'
import { ElInput } from 'element-plus'

const TaskApproval = function(options, backFun, closeBackFun) {
  return new Promise((resolve) => {
    // 实例化组件，createApp第二个参数是props
    const instance = createApp(component, {
      instanceId: options.instanceId,
      taskId: options.taskId,
      taskVars: options.taskVars,
      backFun: (params) => {
        if (typeof backFun === 'function') {
          backFun(params)
        }
        unmount()
        resolve()
      },
      closeBackFun: () => {
        if (typeof closeBackFun === 'function') {
          closeBackFun()
        }
        unmount()
        resolve()
      }
    })

    // 新挂载容器使用的组件
    instance.use(ElButton)
    instance.use(ElDialog)
    instance.use(ElRow)
    instance.use(ElCol)
    instance.use(ElForm)
    instance.use(ElFormItem)
    instance.use(ElRadioGroup)
    instance.use(ElRadio)
    instance.use(ElInput)

    // 创建一个挂载容器
    const parentNode = document.createElement('div')
    document.body.appendChild(parentNode)

    // 卸载组件
    const unmount = () => {
      instance.unmount()
      document.body.removeChild(parentNode)
    }

    // 挂载组件
    instance.mount(parentNode)
  })
}
export default TaskApproval
