import request from '@/utils/request'
/**
 * 查询执行器
 */
export async function getJobInfoList(data) {
  return request({
    url: '/job/jobInfoController/pageList',
    method: 'post',
    params: data
  })
}
/**
 * 查询枚举类
 */
export async function getStrategyEnum(data) {
  return request({
    url: '/job/jobInfoController/getStrategyEnum',
    method: 'post',
    params: data
  })
}
/**
 * 获取下次执行时间
 */
export async function nextTriggerTime(data) {
  return request({
    url: '/job/jobInfoController/nextTriggerTime',
    method: 'post',
    params: data
  })
}
/**
 * 查询执行器
 */
export async function getJobGroupById(id) {
  return request({
    url: '/job/jobGroupController/loadById?id=' + id,
    method: 'get'
  })
}
/**
 * 新增常量配置功能
 */
export async function addJobInfoInfo(data) {
  return request({
    url: '/job/jobInfoController/add',
    method: 'post',
    params: data
  })
}
/**
 * 更新常量配置功能
 */
export async function updateJobGroupInfo(data) {
  return request({
    url: '/job/jobInfoController/update',
    method: 'post',
    params: data
  })
}
/**
 * 删除 常量配置功能
 */
export async function delJobInfo(id) {
  return request({
    url: '/job/jobInfoController/remove?id=' + id,
    method: 'delete'
  })
}
/**
 * 执行一次
 */
export async function executeOnce(id, executorParam, addressList) {
  return request({
    url: '/job/jobInfoController/trigger?id=' + id + '&executorParam=' + executorParam + '&addressList=' + addressList,
    method: 'get'
  })
}
/**
 * 启动
 */
export async function startJobInfo(id) {
  return request({
    url: '/job/jobInfoController/start?id=' + id,
    method: 'post'
  })
}
/**
 * 停止
 */
export async function stopJobInfo(id) {
  return request({
    url: '/job/jobInfoController/stop?id=' + id,
    method: 'post'
  })
}
