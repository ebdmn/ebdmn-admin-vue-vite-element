import request from '@/utils/request'
/**
 * 查询执行器
 */
export async function getJobGroupList(data) {
  return request({
    url: '/job/jobGroupController/pageList',
    method: 'get',
    params: data
  })
}
/**
 * 查询注册节点
 */
export async function getJobGroupById(id) {
  return request({
    url: '/job/jobGroupController/loadById?id=' + id,
    method: 'get'
  })
}
/**
 * 新增常量配置功能
 */
export async function addJobGroupInfo(data) {
  return request({
    url: '/job/jobGroupController/save',
    method: 'post',
    params: data
  })
}
/**
 * 更新常量配置功能
 */
export async function updateJobGroupInfo(data) {
  return request({
    url: '/job/jobGroupController/update',
    method: 'post',
    params: data
  })
}
/**
 * 删除 常量配置功能
 */
export async function delJobGroupInfo(id) {
  return request({
    url: '/job/jobGroupController/remove?id=' + id,
    method: 'delete'
  })
}
