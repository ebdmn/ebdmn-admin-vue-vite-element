import request from '@/utils/request'
/**
 * 查询诶之
 */
export async function getLogInfoList(data) {
  return request({
    url: '/job/jobLogController/pageList2',
    method: 'post',
    params: data
  })
}
export async function chartInfo(data) {
  return request({
    url: '/job/jobLogController/chartInfo',
    method: 'post',
    params: data
  })
}
/**
 * 根据组查询任务ID
 */
export async function getJobsByGroup(id) {
  return request({
    url: '/job/jobLogController/getJobsByGroup?jobGroup=' + id,
    method: 'get'
  })
}

