import { createApp } from 'vue'
import component from '@demo/views/CreateApp/component.vue'
// const app = createApp({component})
// 提供一个父元素
// const parent = document.createElement('div')
import { ElButton } from 'element-plus'
import { ElDialog } from 'element-plus'
import { ElRow } from 'element-plus'
import { ElForm } from 'element-plus'

const userApp = function(options, backFun, closeBackFun, params) {
  return new Promise((resolve, reject) => {
    // 实例化组件，createApp第二个参数是props
    const instance = createApp(component, {
      title: '提示',
      message: '确认消息',
      confirmBtnText: '确定',
      cancelBtnText: '取消',
      backFun: (params) => {
        console.info('createApp.js >>>>>>>>>>>>> onConfirm')
        backFun(params)
        unmount()
        resolve()
      },
      closeBackFun: () => {
        console.info('createApp.js >>>>>>>>>>>>> onCancel')
        closeBackFun()
        unmount()
        resolve()
      }
    })

    instance.use(ElButton)
    instance.use(ElDialog)
    instance.use(ElRow)
    instance.use(ElForm)

    // 卸载组件
    const unmount = () => {
      instance.unmount()
      document.body.removeChild(parentNode)
    }
    // 创建一个挂载容器
    const parentNode = document.createElement('div')
    document.body.appendChild(parentNode)
    // 挂载组件
    instance.mount(parentNode)
  })
}
export default userApp
