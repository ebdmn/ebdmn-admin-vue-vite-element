import request from '@/utils/request'
/**
 * 查询列表APP文章
 */
export async function getEbdmnAppArticleList(data) {
  return request({
    url: '/mobile/article/getEbdmnAppArticleList',
    method: 'get',
    params: data
  })
}
/**
 * 新增APP文章
 */
export async function addEbdmnAppArticleInfo(data) {
  return request({
    url: '/mobile/article/addEbdmnAppArticleInfo',
    method: 'post',
    params: data
  })
}
/**
 * 根据map 新增APP文章
 */
export async function addEbdmnAppArticleInfoByMap(data) {
  return request({
    url: '/mobile/article/addEbdmnAppArticleInfoByMap',
    method: 'post',
    params: data
  })
}
/**
 * 更新APP文章
 */
export async function updateEbdmnAppArticleInfo(data) {
  return request({
    url: '/mobile/article/updateEbdmnAppArticleInfo',
    method: 'post',
    params: data
  })
}
/**
 * 根据 map APP文章
 */
export async function updateEbdmnAppArticleInfoByMap(data) {
  return request({
    url: '/mobile/article/updateEbdmnAppArticleInfoByMap',
    method: 'post',
    data: JSON.stringify(data)
  })
}
/**
 * 删除 APP文章
 */
export async function delEbdmnAppArticleInfo(dbids) {
  return request({
    url: '/mobile/article/delEbdmnAppArticleInfo?dbids=' + dbids,
    method: 'delete'
  })
}
