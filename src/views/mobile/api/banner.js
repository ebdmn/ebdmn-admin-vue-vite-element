import request from '@/utils/request'
/**
 * 查询列表APP轮播图片
 */
export async function getEbdmnAppBannerList(data) {
  return request({
    url: '/mobile/banner/getEbdmnAppBannerList',
    method: 'get',
    params: data
  })
}
/**
 * 新增APP轮播图片
 */
export async function addEbdmnAppBannerInfo(data) {
  return request({
    url: '/mobile/banner/addEbdmnAppBannerInfo',
    method: 'post',
    params: data
  })
}
/**
 * 根据map 新增APP轮播图片
 */
export async function addEbdmnAppBannerInfoByMap(data) {
  return request({
    url: '/mobile/banner/addEbdmnAppBannerInfoByMap',
    method: 'post',
    params: data
  })
}
/**
 * 更新APP轮播图片
 */
export async function updateEbdmnAppBannerInfo(data) {
  return request({
    url: '/mobile/banner/updateEbdmnAppBannerInfo',
    method: 'post',
    params: data
  })
}
/**
 * 根据 map APP轮播图片
 */
export async function updateEbdmnAppBannerInfoByMap(data) {
  return request({
    url: '/mobile/banner/updateEbdmnAppBannerInfoByMap',
    method: 'post',
    params: data
  })
}
/**
 * 删除 APP轮播图片
 */
export async function delEbdmnAppBannerInfo(dbids) {
  return request({
    url: '/mobile/banner/delEbdmnAppBannerInfo?dbids=' + dbids,
    method: 'delete'
  })
}
