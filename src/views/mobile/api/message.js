import request from '@/utils/request'
/**
 * 查询列表APP通知内容
 */
export async function getEbdmnAppMessageList(data) {
  return request({
    url: '/mobile/message/getEbdmnAppMessageList',
    method: 'get',
    params: data
  })
}
/**
 * 新增APP通知内容
 */
export async function addEbdmnAppMessageInfo(data) {
  return request({
    url: '/mobile/message/addEbdmnAppMessageInfo',
    method: 'post',
    params: data
  })
}
/**
 * 根据map 新增APP通知内容
 */
export async function addEbdmnAppMessageInfoByMap(data) {
  return request({
    url: '/mobile/message/addEbdmnAppMessageInfoByMap',
    method: 'post',
    params: data
  })
}
/**
 * 更新APP通知内容
 */
export async function updateEbdmnAppMessageInfo(data) {
  return request({
    url: '/mobile/message/updateEbdmnAppMessageInfo',
    method: 'post',
    params: data
  })
}
/**
 * 根据 map APP通知内容
 */
export async function updateEbdmnAppMessageInfoByMap(data) {
  return request({
    url: '/mobile/message/updateEbdmnAppMessageInfoByMap',
    method: 'post',
    params: data
  })
}
/**
 * 删除 APP通知内容
 */
export async function delEbdmnAppMessageInfo(dbids) {
  return request({
    url: '/mobile/message/delEbdmnAppMessageInfo?dbids=' + dbids,
    method: 'delete'
  })
}
