import request from '@/utils/request'
/**
 * 查询列表文章类型维护
 */
export async function getEbdmnAppArticleTypeList(data) {
  return request({
    url: '/mobile/articleType/getEbdmnAppArticleTypeList',
    method: 'get',
    params: data
  })
}
/**
 * 新增文章类型维护
 */
export async function addEbdmnAppArticleTypeInfo(data) {
  return request({
    url: '/mobile/articleType/addEbdmnAppArticleTypeInfo',
    method: 'post',
    params: data
  })
}
/**
 * 根据map 新增文章类型维护
 */
export async function addEbdmnAppArticleTypeInfoByMap(data) {
  return request({
    url: '/mobile/articleType/addEbdmnAppArticleTypeInfoByMap',
    method: 'post',
    params: data
  })
}
/**
 * 更新文章类型维护
 */
export async function updateEbdmnAppArticleTypeInfo(data) {
  return request({
    url: '/mobile/articleType/updateEbdmnAppArticleTypeInfo',
    method: 'post',
    params: data
  })
}
/**
 * 根据 map 文章类型维护
 */
export async function updateEbdmnAppArticleTypeInfoByMap(data) {
  return request({
    url: '/mobile/articleType/updateEbdmnAppArticleTypeInfoByMap',
    method: 'post',
    params: data
  })
}
/**
 * 删除 文章类型维护
 */
export async function delEbdmnAppArticleTypeInfo(dbids) {
  return request({
    url: '/mobile/articleType/delEbdmnAppArticleTypeInfo?dbids=' + dbids,
    method: 'delete'
  })
}
