import request from '@/utils/request'

export async function getDatabookByGroupid(groupid) {
  return request({
    url: '/basic/databook/getDatabookByGroupid',
    method: 'get',
    params: {
      groupid: groupid
    }
  })
}
export async function getDatabookByGroupcode(groupcode) {
  return request({
    url: '/basic/databook/getDatabookByGroupcode',
    method: 'get',
    params: {
      groupcode: groupcode
    }
  })
}
export async function addDatabook(data) {
  return request({
    url: '/basic/databook/add',
    method: 'post',
    params: data
  })
}
export async function updateDatabook(data) {
  return request({
    url: '/basic/databook/update',
    method: 'post',
    params: data
  })
}
export async function deleteDatabook(dbid) {
  return request({
    url: '/basic/databook/delete',
    method: 'delete',
    params: {
      dbid: dbid
    }
  })
}
