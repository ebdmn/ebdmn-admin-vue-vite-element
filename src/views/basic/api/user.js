import request from '@/utils/request'

export async function getList() {
  return request({
    url: '/auth/userInfo/getList',
    method: 'get'
  })
}
export async function addInfo(data) {
  return request({
    url: '/auth/userInfo/addInfo',
    method: 'post',
    params: data
  })
}
export async function addInfoByMap(data) {
  return request({
    url: '/auth/userInfo/addInfoByMap',
    method: 'post',
    params: data
  })
}
export async function updateInfo(data) {
  return request({
    url: '/auth/userInfo/updateInfo',
    method: 'post',
    params: data
  })
}
export async function updateInfoByMap(data) {
  return request({
    url: '/auth/userInfo/updateInfoByMap',
    method: 'post',
    params: data
  })
}
export async function delInfo(id) {
  return request({
    url: '/auth/userInfo/delInfo',
    method: 'delete',
    params: {
      id: id
    }
  })
}
