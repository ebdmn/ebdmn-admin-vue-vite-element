import request from '@/utils/request'

export async function getPermissionByParentid(parentid) {
  return request({
    url: '/auth/permission/getPermissionByParentid',
    method: 'get',
    params: {
      parentid: parentid
    }
  })
}
export async function addPermission(data) {
  return request({
    url: '/auth/permission/add',
    method: 'post',
    params: data
  })
}
export async function updatePermission(data) {
  return request({
    url: '/auth/permission/update',
    method: 'post',
    params: data
  })
}
export async function deletePermission(dbid) {
  return request({
    url: '/auth/permission/delete',
    method: 'delete',
    params: {
      dbid: dbid
    }
  })
}
