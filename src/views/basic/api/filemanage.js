import request from '@/utils/request'

export async function findAll() {
  return request({
    url: '/fileservice/files/findFiles',
    method: 'get',
    params: {
      page: 1,
      limit: 10
    }
  })
}
export async function deleteFile(id) {
  return request({
    url: '/fileservice/files/deleteFile/' + id,
    method: 'DELETE'
  })
}
