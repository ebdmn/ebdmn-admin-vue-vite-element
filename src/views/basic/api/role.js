import request from '@/utils/request'
/**
 * 查询列表角色
 */
export async function getEbdmnBasicRoleList(data) {
  return request({
    url: '/auth/role/getEbdmnAuthRoleList',
    method: 'get',
    params: data
  })
}
/**
 * 新增角色
 */
export async function addEbdmnBasicRoleInfo(data) {
  return request({
    url: '/auth/role/addEbdmnAuthRoleInfo',
    method: 'post',
    params: data
  })
}
/**
 * 根据map 新增角色
 */
export async function addEbdmnBasicRoleInfoByMap(data) {
  return request({
    url: '/auth/role/addEbdmnAuthRoleInfoByMap',
    method: 'post',
    params: data
  })
}
/**
 * 更新角色
 */
export async function updateEbdmnBasicRoleInfo(data) {
  return request({
    url: '/auth/role/updateEbdmnAuthRoleInfo',
    method: 'post',
    params: data
  })
}
/**
 * 根据 map 角色
 */
export async function updateEbdmnBasicRoleInfoByMap(data) {
  return request({
    url: '/auth/role/updateEbdmnAuthRoleInfoByMap',
    method: 'post',
    params: data
  })
}
/**
 * 删除 角色
 */
export async function delEbdmnBasicRoleInfo(dbids) {
  return request({
    url: '/auth/role/delEbdmnAuthRoleInfo?dbids=' + dbids,
    method: 'delete'
  })
}
/**
 * 查询所有菜单
 */
export async function getAllModuleByRole(data) {
  return request({
    url: '/auth/role/getAllModuleByRole',
    method: 'get',
    params: data
  })
}
/**
 * 查询角色对应的菜单
 */
export async function getModuleByRole(data) {
  return request({
    url: '/auth/role/getModuleByRole',
    method: 'get',
    params: data
  })
}
/**
 * 查询角色对应的权限
 */
export async function getPermissionByRole(data) {
  return request({
    url: '/auth/role/getPermissionByRole',
    method: 'get',
    params: data
  })
}
/**
 * 保存角色对应的模块数据
 */
export async function saveRoleModules(roleID, moduleIDs) {
  return request({
    url: '/auth/role/saveRoleModules/' + roleID,
    method: 'post',
    data: JSON.stringify({
      moduleIDs: moduleIDs
    })
  })
}
/**
 * 获取角色已分配菜单
 */
export async function modulesTreeDefaultChecked(roleID) {
  return request({
    url: '/auth/role/modulesTreeDefaultChecked/' + roleID,
    method: 'post'
  })
}
