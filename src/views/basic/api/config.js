import request from '@/utils/request'
/**
 * 查询列表常量配置功能
 */
export async function getEbdmnBasicConfigList(data) {
  return request({
    url: '/basic/config/getEbdmnBasicConfigList',
    method: 'get',
    params: data
  })
}
/**
 * 新增常量配置功能
 */
export async function addEbdmnBasicConfigInfo(data) {
  return request({
    url: '/basic/config/addEbdmnBasicConfigInfo',
    method: 'post',
    params: data
  })
}
/**
 * 根据map 新增常量配置功能
 */
export async function addEbdmnBasicConfigInfoByMap(data) {
  return request({
    url: '/basic/config/addEbdmnBasicConfigInfoByMap',
    method: 'post',
    params: data
  })
}
/**
 * 更新常量配置功能
 */
export async function updateEbdmnBasicConfigInfo(data) {
  return request({
    url: '/basic/config/updateEbdmnBasicConfigInfo',
    method: 'post',
    params: data
  })
}
/**
 * 根据 map 常量配置功能
 */
export async function updateEbdmnBasicConfigInfoByMap(data) {
  return request({
    url: '/basic/config/updateEbdmnBasicConfigInfoByMap',
    method: 'post',
    params: data
  })
}
/**
 * 删除 常量配置功能
 */
export async function delEbdmnBasicConfigInfo(configIds) {
  return request({
    url: '/basic/config/delEbdmnBasicConfigInfo?configIds=' + configIds,
    method: 'delete'
  })
}
