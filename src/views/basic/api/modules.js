import request from '@/utils/request'

export async function getModulesByParentid(data) {
  return request({
    url: '/auth/module/getModulesByParentid',
    method: 'get',
    params: {
      parentid: data.parentid
    }
  })
}
export async function getAllModules() {
  return request({
    url: '/auth/module/getAllModules',
    method: 'get'
  })
}
export async function addModules(data) {
  return request({
    url: '/auth/module/add',
    method: 'post',
    params: data
  })
}
export async function updateModules(data) {
  return request({
    url: '/auth/module/update',
    method: 'post',
    params: data
  })
}
export async function deleteModules(dbid) {
  return request({
    url: '/auth/module/delete',
    method: 'delete',
    params: {
      moduleID: dbid
    }
  })
}
