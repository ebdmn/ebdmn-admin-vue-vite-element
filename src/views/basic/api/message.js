import request from '@/utils/request'
/**
 * 查询列表待办消息
 */
export async function getEbdmnMsgMessageList(data) {
  return request({
    url: '/message/msgController/getEbdmnMsgMessageList',
    method: 'get',
    params: data
  })
}/**
 * 查询列表待办消息
 */
export async function getUserNoticeList(data) {
  return request({
    url: '/message/msgController/getUserNoticeList',
    method: 'get',
    params: data
  })
}
/**
 * 新增待办消息
 */
export async function addEbdmnMsgMessageInfo(data) {
  return request({
    url: '/message/msgController/addEbdmnMsgMessageInfo',
    method: 'post',
    params: data
  })
}
/**
 * 根据map 新增待办消息
 */
export async function addEbdmnMsgMessageInfoByMap(data) {
  return request({
    url: '/message/msgController/addEbdmnMsgMessageInfoByMap',
    method: 'post',
    params: data
  })
}
/**
 * 更新待办消息
 */
export async function updateEbdmnMsgMessageInfo(data) {
  return request({
    url: '/message/msgController/updateEbdmnMsgMessageInfo',
    method: 'post',
    params: data
  })
}
/**
 * 根据 map 待办消息
 */
export async function updateEbdmnMsgMessageInfoByMap(data) {
  return request({
    url: '/message/msgController/updateEbdmnMsgMessageInfoByMap',
    method: 'post',
    params: data
  })
}
/**
 * 根据 map 更新待办
 */
export async function changeMsgMessageBoxStatus(data) {
  return request({
    url: '/message/msgController/changeMsgMessageBoxStatus',
    method: 'post',
    params: data
  })
}
/**
 * 删除 待办消息
 */
export async function delEbdmnMsgMessageInfo(dbids) {
  return request({
    url: '/message/msgController/delEbdmnMsgMessageInfo?dbids=' + dbids,
    method: 'delete'
  })
}
