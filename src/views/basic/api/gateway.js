import request from '@/utils/request'

export async function findAll() {
  return request({
    url: '/gateway/route/findAll',
    method: 'get',
    params: {
      page: 0,
      limit: 10000
    }
  })
}
export async function synchronization(data) {
  return request({
    url: '/gateway/route/synchronization',
    method: 'get'
  })
}
export async function addInfoByMap(data) {
  return request({
    url: '/basic/userInfo/addInfoByMap',
    method: 'post',
    params: data
  })
}
export async function updateInfo(data) {
  return request({
    url: '/basic/userInfo/updateInfo',
    method: 'post',
    params: data
  })
}
export async function updateInfoByMap(data) {
  return request({
    url: '/basic/userInfo/updateInfoByMap',
    method: 'post',
    params: data
  })
}
export async function delInfo(id) {
  return request({
    url: '/basic/userInfo/delInfo',
    method: 'delete',
    params: {
      id: id
    }
  })
}
