
// import configSettings from '@/config'
import settings from '@config/index'
// const {
//     title
// } = configSettings
// const title = settings.title
// const theme = settings.theme

const state = () => ({
  // title,
  // theme
  ...settings
})
const getters = {
  title: state => state.app.title,
  theme: state => state.app.theme,
  copyright: state => state.app.copyright
}
const mutations = {
  SET_THEME(state, theme) {
    state.app.theme = theme
  }
}
const actions = {
  changeTheme({ commit }, theme) {
    commit('SET_THEME', theme)
  }
}
export default { namespaced: true, state, getters, mutations, actions }
