/**
 * @description tagsBar多标签页逻辑，前期借鉴了很多开源项目发现都有个共同的特点很繁琐并不符合框架设计的初衷，
 */

const state = () => ({
  visitedRoutes: [],
  cachedRoutes: []
})
const getters = {
  visitedRoutes: state => state.visitedRoutes,
  cachedRoutes: state => state.cachedRoutes
}
const mutations = {
  /**
   * @description 添加标签页
   * @param {*} state
   * @param {*} route
   * @returns
   */
  ADD_TAGS_VISITED_ROUTE(state, route) {
    const target = state.visitedRoutes.find(item => item.path === route.path)
    if (target) {
      if (route.fullPath !== target.fullPath) Object.assign(target, route)
      return
    }
    state.visitedRoutes.push(Object.assign({}, route))
  },
  ADD_CACHED_ROUTE: (state, view) => {
    if (state.cachedRoutes.includes(view.name)) return
    if (!view.meta.noCache) {
      state.cachedRoutes.push(view.name)
    }
  },
  /**
   * @description 删除当前标签页
   * @param {*} state
   * @param {*} route
   * @returns
   */
  DEL_TAGS_VISITED_ROUTE(state, route) {
    state.visitedRoutes.forEach((item, index) => {
      if (item.path === route.path) state.visitedRoutes.splice(index, 1)
    })
  },
  DEL_CACHED_ROUTE: (state, view) => {
    const index = state.cachedRoutes.indexOf(view.name)
    index > -1 && state.cachedRoutes.splice(index, 1)
  },
  /**
   * @description 删除当前标签页以外其它全部多标签页
   * @param {*} state
   * @param {*} route
   * @returns
   */
  DEL_TAGS_OTHERS_VISITED_ROUTE(state, route) {
    state.visitedRoutes = state.visitedRoutes.filter(
      item => item.meta.affix || item.path === route.path
    )
  },
  DEL_OTHERS_CACHED_ROUTE: (state, view) => {
    const index = state.cachedRoutes.indexOf(view.name)
    if (index > -1) {
      state.cachedRoutes = state.cachedRoutes.slice(index, index + 1)
    } else {
      // if index = -1, there is no cached tags
      state.cachedRoutes = []
    }
  },
  /**
   * @description 删除当前标签页左边全部多标签页
   * @param {*} state
   * @param {*} route
   * @returns
   */
  DEL_TAGS_LEFT_VISITED_ROUTE(state, route) {
    let index = state.visitedRoutes.length
    state.visitedRoutes = state.visitedRoutes.filter(item => {
      if (item.name === route.name) index = state.visitedRoutes.indexOf(item)
      return item.meta.affix || index <= state.visitedRoutes.indexOf(item)
    })
  },
  DEL_LEFT_CACHED_ROUTE: (state, view) => {
    const currAndRightCachedView = []
    let currCacheViewIsGet = false
    state.cachedRoutes.forEach(v => {
      v === view.name && (currCacheViewIsGet = true)
      currCacheViewIsGet && (currAndRightCachedView.push(v))
    })
    state.visitedViews.forEach(v => {
      v.meta.affix && !currAndRightCachedView.includes(v.name) && (currAndRightCachedView.unshift(v.name))
    })
    state.cachedRoutes = currAndRightCachedView
  },
  /**
   * @description 删除当前标签页右边全部多标签页
   * @param {*} state
   * @param {*} route
   * @returns
   */
  DEL_TAGS_RIGHT_VISITED_ROUTE(state, route) {
    let index = state.visitedRoutes.length
    state.visitedRoutes = state.visitedRoutes.filter(item => {
      if (item.name === route.name) index = state.visitedRoutes.indexOf(item)
      return item.meta.affix || index >= state.visitedRoutes.indexOf(item)
    })
  },
  DEL_RIGHT_CACHED_ROUTE: (state, view) => {
    const leftAndCurrCachedView = []
    let currCacheViewIsGet = false
    state.cachedRoutes.forEach(v => {
      !currCacheViewIsGet && (leftAndCurrCachedView.push(v))
      v === view.name && (currCacheViewIsGet = true)
    })
    state.visitedRoutes.forEach(v => {
      v.meta.affix && !leftAndCurrCachedView.includes(v.name) && (leftAndCurrCachedView.unshift(v.name))
    })
    state.cachedRoutes = leftAndCurrCachedView
  },
  /**
   * @description 删除全部多标签页
   * @param {*} state
   * @param {*} route
   * @returns
   */
  DEL_TAGS_ALL_VISITED_ROUTE(state) {
    state.visitedRoutes = state.visitedRoutes.filter(item => item.meta.affix)
  },

  DEL_ALL_CACHED_ROUTE: state => {
    state.cachedRoutes = []
  }
}
const actions = {
  /**
   * @description 添加标签页
   * @param {*} { commit }
   * @param {*} route
   */
  addVisitedRoute({ commit }, route) {
    commit('ADD_TAGS_VISITED_ROUTE', route)
    commit('ADD_CACHED_ROUTE', route)
  },
  addCachedRoute({ commit }, route) {
    commit('ADD_CACHED_ROUTE', route)
  },
  /**
   * @description 删除当前标签页
   * @param {*} { commit }
   * @param {*} route
   */
  delVisitedRoute({ commit }, route) {
    commit('DEL_TAGS_VISITED_ROUTE', route)
    commit('DEL_CACHED_ROUTE', route)
  },
  delCachedRoute({ commit }, route) {
    commit('DEL_CACHED_ROUTE', route)
  },
  /**
   * @description 删除当前标签页以外其它全部多标签页
   * @param {*} { commit }
   * @param {*} route
   */
  delOthersVisitedRoutes({ commit }, route) {
    commit('DEL_TAGS_OTHERS_VISITED_ROUTE', route)
    commit('DEL_OTHERS_CACHED_ROUTE', route)
  },
  delOtherscACHEDRoutes({ commit }, route) {
    commit('DEL_OTHERS_CACHED_ROUTE', route)
  },
  /**
   * @description 删除当前标签页左边全部多标签页
   * @param {*} { commit }
   * @param {*} route
   */
  delLeftVisitedRoutes({ commit }, route) {
    commit('DEL_TAGS_LEFT_VISITED_ROUTE', route)
    commit('DEL_LEFT_CACHED_ROUTE', route)
  },
  delLeftCachedRoutes({ commit }, route) {
    commit('DEL_LEFT_CACHED_ROUTE', route)
  },
  /**
   * @description 删除当前标签页右边全部多标签页
   * @param {*} { commit }
   * @param {*} route
   */
  delRightVisitedRoutes({ commit }, route) {
    commit('DEL_TAGS_RIGHT_VISITED_ROUTE', route)
    commit('DEL_RIGHT_CACHED_ROUTE', route)
  },
  delRightCachedRoutes({ commit }, route) {
    commit('DEL_RIGHT_CACHED_ROUTE', route)
  },
  /**
   * @description 删除全部多标签页
   * @param {*} { commit }
   */
  delAllVisitedRoutes({ commit }) {
    commit('DEL_TAGS_ALL_VISITED_ROUTE')
    commit('DEL_ALL_CACHED_ROUTE')
  },
  delAllCachedRoutes({ commit }) {
    commit('DEL_ALL_CACHED_ROUTE')
  }
}
export default { namespaced: true, state, getters, mutations, actions }
