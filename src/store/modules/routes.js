/**
 *
 * @description 路由拦截状态管理，目前两种模式：all模式与intelligence模式
 */
import { asyncRoutes, constantRoutes } from '@/router'
import { getRouterList } from '@/api/router'
import { convertRouter, filterRoutes } from '@/utils/routes'

const state = () => ({
  routes: []
})
const getters = {
  routes: state => state.routes
}
const mutations = {
  SET_ROUTES(state, routes) {
    state.routes = routes
  }
}
const actions = {
  /**
   * @description intelligence模式设置路由
   * @param {*} { commit }
   * @returns
   */
  async setRoutes({ commit }) {
    const finallyRoutes = filterRoutes([...constantRoutes, ...asyncRoutes])
    commit('SET_ROUTES', finallyRoutes)
    return [...asyncRoutes]
  },
  /**
   * @description all模式设置路由
   * @param {*} { commit }
   * @returns
   */
  async setAllRoutes({ commit }) {
    const response = await getRouterList()
    let data = []
    data = response.dataSets
    data.push({ path: '/:catchAll(.*)', redirect: '/404', hidden: true })
    data.unshift({
      path: '/',
      component: 'Layout',
      redirect: '/index',
      meta: {
        title: '工作台',
        icon: 'desktop',
        affix: true
      },
      children: [
        {
          path: '/index',
          name: 'Index',
          component: '/index',
          meta: {
            title: '工作台',
            icon: 'dashboard',
            affix: true
          }
        }
      ]
    })
    const asyncRoutes = convertRouter(data)
    const finallyRoutes = filterRoutes([...constantRoutes, ...asyncRoutes])
    commit('SET_ROUTES', finallyRoutes)
    return [...asyncRoutes]
  }
}
export default { namespaced: true, state, getters, mutations, actions }
