
import { createStore } from 'vuex'

const modules = {}
/* eslint-disable */
const modulesContext = import.meta.globEager('./modules/*.js')
/* eslint-enable */
for (const path in modulesContext) {
  modules[path.slice(10, -3)] = modulesContext[path].default
}
export default createStore({
  modules: modules
  // strict: !import.meta.env.PROD,
  // plugins: !import.meta.env.PROD ? [createLogger()] : []
})
