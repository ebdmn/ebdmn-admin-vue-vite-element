// import Vue from 'vue'
import App from './App.vue'
import { createApp } from 'vue'
import router from './router'
import store from './store'
// 自定义权限
import '@/ebdmn/plugins/permissions'
import ElementPlus from 'element-plus'
import * as icons from '@element-plus/icons-vue'
import 'element-plus/dist/index.css'
// 复制插件
import VueClipboard from 'vue-clipboard2'
// Vue.config.productionTip = false
// 自定义样式
import '@styles/ebdmn-styles/index.scss'
// 注册指令
import plugins from './plugins' // plugins
// 下载文件方法(通过注册指令来实现了)
// import { download } from '@/utils/request'

// If you want to use ElMessage, import it.
import 'element-plus/theme-chalk/src/message.scss'
import * as echarts from 'echarts'

const app = createApp(App)
  .use(store)
  .use(router)
  .use(ElementPlus)
  .use(plugins)
  .use(VueClipboard)
app.config.globalProperties.echarts = echarts
// app.config.globalProperties.download = download

Object.keys(icons).forEach(key => {
  app.component(key, icons[key])
})
app.mount('#app')
