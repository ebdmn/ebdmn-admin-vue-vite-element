import banner from 'vite-plugin-banner'
// 当你在你的项目上运行 npm run build 打包的时候，在 dist 文件夹（或者你在 vite.config.ts 配置的其他 build.outDir ），
// 除了 vendor 文件外，所有的 js 和 css 文件都会添加一个 banner 注释在文件顶部。
export default function createBanner() {
  return banner(`
    /**
 * 由 Ebdmn 提供技术支持
 * https://gitee.com/ebdmn/ebdmn-admin-vue-vite-element
 * Powered by 二把刀码农
 * https://gitee.com/ebdmn/ebdmn-admin-vue-vite-element
 */
 `)
}
