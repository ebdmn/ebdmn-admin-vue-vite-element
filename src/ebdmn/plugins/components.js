// 按需加载
// 你同样可以使用 unplugin-vue-components
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
export default function createElementPlusResolver() {
  return Components({
    resolvers: [
      ElementPlusResolver({
        importStyle: 'sass'
        // directives: true,
        // version: "1.2.0-beta.1",
      })
    ]
  })
}
