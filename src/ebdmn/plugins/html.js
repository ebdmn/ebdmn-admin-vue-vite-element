// 浏览器控制台信息打印
import htmlPlugin from 'vite-plugin-html'

const copyright_common_style = 'font-size: 14px; margin-bottom: 2px; padding: 6px 8px; color: #fff;'
const copyright_main_style = `${copyright_common_style} background: #0099ff;`
const copyright_sub_style = `${copyright_common_style} background: #707070;`

export default function createHtml(env, isBuild = false) {
  const { VITE_APP_TITLE, VITE_APP_DEBUG_TOOL, VITE_APP_MODE } = env
  const html = htmlPlugin({
    inject: {
      injectData: {
        title: VITE_APP_TITLE,
        debugTool: VITE_APP_DEBUG_TOOL,
        appMode: VITE_APP_MODE,
        copyrightScript: `
<script>
console.info('%c由%cEbdmn%c提供技术支持', '${copyright_sub_style}', '${copyright_main_style}', '${copyright_sub_style}', '\\nhttps://gitee.com/xiaohui987/ebdmn-admin-vue-vite-element');
console.info('%cPowered by%c二把刀码农', '${copyright_sub_style}', '${copyright_main_style}', '\\nhttps://gitee.com/xiaohui987/ebdmn-admin-vue-vite-element');
</script>
                `
      }
    },
    minify: isBuild
  })
  return html
}
