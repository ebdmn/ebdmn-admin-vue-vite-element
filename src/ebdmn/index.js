// 加载插件
// const requirePlugin = import.meta.glob('./plugins/*.js')
// for (const path in requirePlugin) {
//     requirePlugin[path]
// }
import vue from '@vitejs/plugin-vue'

import createHtml from './plugins/html'
import createBanner from './plugins/banner'
// 按需加载插件
// import createElementPlusResolver from './plugins/components'
// import createAutoImport from './plugins/components'

export default function createVitePlugins(env, command) {
  const vitePlugins = [vue()]
  vitePlugins.push(createHtml(env, command))
  vitePlugins.push(createBanner())
  // vitePlugins.push(createComponents())
  // vitePlugins.push(createAutoImport())
  return vitePlugins
}
