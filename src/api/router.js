import request from '@/utils/request'

// 获取路由数据
export function getRouterList(params) {
  return request({
    url: '/auth/module/initModule',
    method: 'get',
    params
  })
}
