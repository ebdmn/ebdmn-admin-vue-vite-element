import request from '@/utils/request'

export async function getData(url, method, params, data) {
  return request({
    url: url,
    method: method,
    params: params,
    data: data
  })
}
