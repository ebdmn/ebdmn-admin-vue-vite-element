import request from '@/utils/request'
// import Base64 from '@vitejs/plugin-vue'

export async function login(data) {
  // const tok = 'ebdmn:secret'
  // const hash = Base64.encode(tok)
  // const Basic = 'Basic ' + hash
  return request({
    url: '/auth/oauth2/token?grant_type=password&scope=userinfo',
    method: 'post',
    params: {
      username: data.username,
      password: data.password
    },
    headers: {
      'Authorization': 'Basic ZWJkbW46c2VjcmV0'
    }
  })
}

export async function socialLogin(data) {
  return request({
    url: '/socialLogin',
    method: 'post',
    data
  })
}

export function getLoginUserInfo() {
  return request({
    url: '/auth/userInfo/getUserInfo',
    method: 'get',
    data: {
      // [tokenName]: accessToken,
    }
  })
  // const data = {roles: ["admin"], authorities:["READ", "WRITE", "DELETE"], username: 'admin', avatar: 'https://i.gtimg.cn/club/item/face/img/8/15918_100.gif'}
  // return data
}

export function logout() {
  return request({
    url: '/logout',
    method: 'post'
  })
}

export function register() {
  return request({
    url: '/register',
    method: 'post'
  })
}
