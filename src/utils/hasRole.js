import store from '@/store'

export function hasRole(value) {
  if (store.getters['user/admin']) return true
  if (value instanceof Array && value.length > 0) {
    return can(store.getters['user/role'], {
      role: value,
      mode: 'oneOf'
    })
  }
  let mode = 'oneOf'
  if (Object.prototype.hasOwnProperty.call(value, 'mode')) mode = value['mode']
  let result = true
  if (Object.prototype.hasOwnProperty.call(value, 'role')) {
    result =
      result && can(store.getters['user/role'], { role: value['role'], mode })
  }
  if (result && Object.prototype.hasOwnProperty.call(value, 'authorities')) {
    result =
      result &&
      can(store.getters['user/authorities'], {
        role: value['authorities'],
        mode
      })
  }
  return result
}

export function can(roleOrAuthorities, value) {
  let hasRole = false
  if (
    value instanceof Object &&
    Object.prototype.hasOwnProperty.call(value, 'role') &&
    Object.prototype.hasOwnProperty.call(value, 'mode')
  ) {
    const { role, mode } = value
    if (mode === 'allOf') {
      hasRole = role.every(item => {
        return roleOrAuthorities.includes(item)
      })
    }
    if (mode === 'oneOf') {
      hasRole = role.some(item => {
        return roleOrAuthorities.includes(item)
      })
    }
    if (mode === 'except') {
      hasRole = !role.some(item => {
        return roleOrAuthorities.includes(item)
      })
    }
  }
  return hasRole
}
