import axios from 'axios'
import settings from '@config/index'
import store from '@/store'
import qs from 'qs'
import router from '@/router'
import { ElMessage, ElLoading } from 'element-plus'
import { tansParams, blobValidate } from '@/utils/ebdmn.js'
import { saveAs } from 'file-saver'

let loadingInstance
let downloadLoadingInstance
/**
 * @description 处理code异常
 * @param {*} code
 * @param {*} msg
 */
const handleCode = (code, msg) => {
  switch (code) {
    case 401:
      ElMessage.error(msg || '登录失效')
      store.dispatch('user/resetAll').catch(() => {})
      break
    case 403:
      router.push({ path: '/401' }).catch(() => {})
      break
    default:
      ElMessage.error(`后端接口${code}异常`)
      ElMessage.error(msg)
      break
  }
}

/**
 *
 * @description axios初始化
 */
const instance = axios.create({
  timeout: settings.network.requestTimeout,
  headers: {
    'Content-Type': settings.network.contentType
  }
})

/**
 * @description axios请求拦截器
 */
instance.interceptors.request.use(
  config => {
    if (store.getters['user/accessToken'])
    // config.headers[tokenName] = store.getters['user/accessToken']
    { config.headers[settings.auth.tokenName] = 'Bearer ' + store.getters['user/accessToken'] }
    if (
      config.data &&
      config.headers['Content-Type'] ===
        'application/x-www-form-urlencoded;charset=UTF-8'
    ) { config.data = qs.stringify(config.data) }
    if (settings.debounce.some(item => config.url.includes(item))) {
      // 这里写加载动画
    }
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

/**
 *
 * @description axios响应拦截器
 */
instance.interceptors.response.use(
  response => {
    if (settings.loadingInstance) loadingInstance.close()
    const { data, config } = response
    let { success } = data
    const { message, code } = data
    // 操作正常Code数组
    // const codeVerificationArray = isArray(successCode)
    //   ? [...successCode]
    //   : [...[successCode]]
    // 是否操作正常
    if (!success) {
      const index = config.url.indexOf(`oauth2/token`)
      if (index > -1) {
        success = true
      }
    }
    if (success === true) {
      return data
    } else {
      handleCode(code, message)
      return Promise.reject(
        '请求异常拦截:' +
          JSON.stringify({ url: config.url, code, message }) || 'Error'
      )
    }
  },
  error => {
    if (settings.loadingInstance) loadingInstance.close()
    const { response, message } = error
    if (error.response && error.response.data) {
      const { status, data } = response
      handleCode(status, data.msg || message)
      return Promise.reject(error)
    } else {
      let { message } = error
      if (message === 'Network Error') {
        message = '后端接口连接异常'
      }
      if (message.includes('timeout')) {
        message = '后端接口请求超时'
      }
      if (message.includes('Request failed with status code')) {
        const code = message.substr(message.length - 3)
        message = '后端接口' + code + '异常'
      }
      ElMessage.error(message || '后端接口未知异常')
      return Promise.reject(error)
    }
  }
)
// 通用下载方法
export function download(url, params, filename) {
  downloadLoadingInstance = ElLoading.service({ text: '正在下载数据，请稍候', background: 'rgba(0, 0, 0, 0.7)' })
  return instance.post(url, params, {
    transformRequest: [(params) => { return tansParams(params) }],
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    responseType: 'blob'
  }).then(async(data) => {
    const isLogin = await blobValidate(data)
    if (isLogin) {
      const blob = new Blob([data])
      saveAs(blob, filename)
    } else {
      const resText = await data.text()
      const rspObj = JSON.parse(resText)
      const errMsg = rspObj.message
      ElMessage.error(errMsg)
    }
    downloadLoadingInstance.close()
  }).catch((r) => {
    console.error(r)
    ElMessage.error('下载文件出现错误，请联系管理员！')
    downloadLoadingInstance.close()
  })
}
export default instance
