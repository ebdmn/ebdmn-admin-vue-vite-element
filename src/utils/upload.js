import axios from 'axios'
import store from '@/store'
import { ElMessage, ElMessageBox } from 'element-plus'

const _baseURL = '/fileservice' // process.env.VUE_APP_BASE_API
const uploadFileType = 'multipart/form-data'

/**
 * 上传文件的请求
 * @param url
 * @returns {AxiosPromise}
 */
export function uploadFile(url, data, uploader) {
  const config = {
    // 请求的接口，在请求的时候，如axios.get(url,config);这里的url会覆盖掉config中的url
    url: url,
    // 基础url前缀
    baseURL: _baseURL, // '',
    transformResponse: [function(data1) {
      var data = data1
      if (typeof data1 === 'string') {
        data = JSON.parse(data1)
      }
      const res = data
      if (!res.success) {
        ElMessage({
          message: res.exception || 'error',
          type: 'error',
          duration: 5 * 1000
        })
        if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
          // 请自行在引入 MessageBox
          // import { Message, MessageBox } from 'element-ui'
          ElMessageBox.confirm(
            '你已被登出，可以取消继续留在该页面，或者重新登录',
            '确定登出', {
              confirmButtonText: '重新登录',
              cancelButtonText: '取消',
              type: 'warning'
            }
          ).then(() => {
            store.dispatch('tpridmp_common_user/resetToken').then(() => {
              location.reload() // 为了重新实例化vue-router对象 避免bug
            })
          })
        }
        return Promise.reject(res.exception || 'error')
      } else {
        return data
      }
    }],
    // 请求头信息
    headers: {
      'Authorization': 'Bearer ' + store.getters['user/accessToken'],
      'Content-Type': uploadFileType
    },
    // processCb进度条回调
    onUploadProgress: progressEvent => {
      const percent = (progressEvent.loaded / progressEvent.total * 100) | 0
      // 调用onProgress方法来显示进度条，需要传递个对象 percent为进度值
      uploader.onProgress({ percent: percent })
    },
    // 跨域请求时是否需要使用凭证
    withCredentials: true,
    contentType: false,
    // 返回数据类型
    responseType: 'json' // default
  }
  return axios.post(url, data, config)
}
