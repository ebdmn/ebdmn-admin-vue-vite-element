import config from '@config'

/**
 * @description 设置标题
 * @param pageTitle
 * @returns {string}
 */
export default function getPageTitle(pageTitle) {
  let newTitles = []
  if (pageTitle) newTitles.push(pageTitle)
  if (config.app.title) newTitles.push(config.app.title)
  if (config.app.titleReverse) newTitles = newTitles.reverse()
  return newTitles.join(config.app.titleSeparator)
}
