import router from '@/router'
import path from 'path-browserify'
import { isExternal } from '@/utils/validate'
/* eslint-disable */
const modules = import.meta.glob('../views/**/*.vue')
/* eslint-enable */
import Layout from '@layout/index.vue'
/**
 * @description all模式渲染后端返回路由
 * @param constantRoutes
 * @returns {*}
 */
export function convertRouter(constantRoutes) {
  return constantRoutes.map(route => {
    if (route.component) {
      if (route.component === 'Layout') {
        route.component = Layout
      } else {
        let path = 'views/' + route.component
        if (
          new RegExp('^/views/.*$').test(route.component) ||
          new RegExp('^views/.*$').test(route.component)
        ) {
          path = route.component
        } else if (new RegExp('^/.*$').test(route.component)) {
          path = 'views' + route.component
        } else if (new RegExp('^@views/.*$').test(route.component)) {
          path = route.component.slice(1)
        } else {
          path = 'views/' + route.component
        }
        let component = modules[`../${path}.vue`]
        if (!component) {
          component = modules[`../${path}/index.vue`]
        }
        route.component = component
      }
    } else {
      route.component = Layout
    }
    if (route.children && route.children.length)
      route.children = convertRouter(route.children)

    if (route.children && route.children.length === 0) delete route.children
    return route
  })
}

/**
 * @description 根据拦截路由
 * @param routes
 * @param baseUrl
 * @returns {[]}
 */
export function filterRoutes(routes, baseUrl = '/') {
  const routes_new = routes
    .filter(() => {
      return true
    })
    .map(route => {
      if (route.path !== '*' && !isExternal(route.path))
        route.path = path.resolve(baseUrl, route.path)
      route.fullPath = route.path
      if (route.children)
        route.children = filterRoutes(route.children, route.fullPath)
      return route
    })
  return routes_new
}

/**
 * 根据当前页面firstMenu
 * @returns {string}
 */
export function handleFirstMenu() {
  const firstMenu = router.currentRoute.matched[0].path
  if (firstMenu === '') return '/'
  return firstMenu
}
