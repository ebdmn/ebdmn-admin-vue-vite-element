// import { storage, tokenTableName } from '@config'
import settings from '@config/index'
import cookie from 'js-cookie'

/**
 * @description 获取accessToken
 * @returns {string|ActiveX.IXMLDOMNode|Promise<any>|any|IDBRequest<any>|MediaKeyStatus|FormDataEntryValue|Function|Promise<Credential | null>}
 */
export function getAccessToken() {
  if (settings.storage) {
    if (settings.storage === 'localStorage') {
      return localStorage.getItem(settings.auth.tokenTableName)
    } else if (settings.storage === 'sessionStorage') {
      return sessionStorage.getItem(settings.auth.tokenTableName)
    } else if (settings.storage === 'cookie') {
      return cookie.get(settings.auth.tokenTableName)
    } else {
      return localStorage.getItem(settings.auth.tokenTableName)
    }
  } else {
    return localStorage.getItem(settings.auth.tokenTableName)
  }
}

/**
 * @description 存储accessToken
 * @param accessToken
 * @returns {void|*}
 */
export function setAccessToken(accessToken) {
  if (settings.storage) {
    if (settings.storage === 'localStorage') {
      return localStorage.setItem(settings.auth.tokenTableName, accessToken)
    } else if (settings.storage === 'sessionStorage') {
      return sessionStorage.setItem(settings.auth.tokenTableName, accessToken)
    } else if (settings.storage === 'cookie') {
      return cookie.set(settings.auth.tokenTableName, accessToken)
    } else {
      return localStorage.setItem(settings.auth.tokenTableName, accessToken)
    }
  } else {
    return localStorage.setItem(settings.auth.tokenTableName, accessToken)
  }
}

/**
 * @description 移除accessToken
 * @returns {void|Promise<void>}
 */
export function removeAccessToken() {
  if (settings.storage) {
    if (settings.storage === 'localStorage') {
      return localStorage.removeItem(settings.auth.tokenTableName)
    } else if (settings.storage === 'sessionStorage') {
      return sessionStorage.clear()
    } else if (settings.storage === 'cookie') {
      return cookie.remove(settings.auth.tokenTableName)
    } else {
      return localStorage.removeItem(settings.auth.tokenTableName)
    }
  } else {
    return localStorage.removeItem(settings.auth.tokenTableName)
  }
}
