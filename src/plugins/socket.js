/**
 * 全局websocket通信单一实例长连接组件
 * 功能：项目主文件执行时生成websocket实例,vue文件中通过this.$websocket调用
 * obj = {
 *   onopen: ()=> {},
 *   onerror: ()=> {},
 *   onmessage: ()=> {},
 *   onclose: ()=> {},
 * }
 * 订阅消息API：this.$websocket.addSubListener(obj)
 * 取消订阅API: this.$websocket.removeSubListener(obj)
 * @author wwb 2021/12/07
 */

import { ElMessage } from 'element-plus'
import SnowflakeId from 'snowflake-id'
const config = {
  socketInstance: null,
  url: '',
  subs: [],
  time: 1000 * 3, // 客户端重新连接频率
  serverTimers: 1000 * 30, // 服务器连接超时时间、心跳侦听频率
  interVal: null,
  serverTimeoutTask: null
}
export default {
  init() {
    if (
      !WebSocket ||
        Object.prototype.toString.call(WebSocket) !== '[object Function]'
    ) {
      ElMessage({
        message: '浏览器版本太低，WebSocket功能不能使用!',
        type: 'error',
        duration: 5 * 1000
      })
    }
    this.setMessageUrl()
    if (!config.socketInstance) {
      try {
        if (config.url) {
          config.socketInstance = new WebSocket(config.url)

          // 监听socket连接
          config.socketInstance.onopen = () => {
            console.info('webSocket连接成功！')
            this.findEvent('onopen')
          }
          // 监听socket错误信息
          config.socketInstance.onerror = error => {
            this.onError(error)
            this.findEvent('onerror', error)
          }
          // 监听socket消息
          config.socketInstance.onmessage = msg => {
            // debugger
            const data = JSON.parse(msg.data)
            if (data.state === 'up') {
              if (data.type === 'heartbeat') {
                config.serverTimeoutTask && clearTimeout(config.serverTimeoutTask)
                return
              }
            }
            const msgData = JSON.parse(msg.data)
            this.findEvent('onmessage', msgData)
          }
          // 监听socket关闭
          config.socketInstance.onclose = () => {
            this.findEvent('onclose')
          }
          config.interVal = setInterval(() => {
            this.checkConnectStatus(config.socketInstance)
          }, config.serverTimers)
        }
      } catch (error) {
        console.error(error)
      }
    }
  },
  /*
     * 设置webSocket连接地址
     */
  setMessageUrl() {
    // debugger
    if (config.url) {
      return
    }
    const local = window.location
    let prepath = `${local.protocol === 'https:' ? 'wss:' : 'ws:'}//${
      local.host
    }`
    const username = localStorage.getItem('username')
    // const code = 'admin'
    if (username) {
      const sid = new SnowflakeId()
      const id = sid.generate()
      prepath += local.pathname + 'websocket/webSocket/' + username + '/' + id
      console.info('websocket url------', prepath)
      config.url = prepath
    } else {
      setTimeout(() => {
        this.init()
      }, config.time)
    }
  },
  /*
     * websocket发布消息
     * @params {evType} 消息类型
     */
  findEvent(evType) {
    config.subs.map(v => {
      if (
        v[evType] &&
          Object.prototype.toString.call(v[evType]) === '[object Function]'
      ) {
        v[evType].apply(null, [...arguments].slice(1))
      }
    })
  },
  /*
     * 客户端发送消息
     * @params {msg} 向服务器发送的消息
     */
  sendHeartbeat(msg) {
    if (config.socketInstance && config.socketInstance.readyState === 1) {
      if (typeof msg === 'string') {
        config.socketInstance.send(msg)
      } else {
        ElMessage({
          message: 'WebSocket客户端只能发送文本字符串格式信息!',
          type: 'error',
          duration: 5 * 1000
        })
      }
    }
  },
  /*
     * 添加订阅事件
     * @params {obj} 订阅方对象
     */
  addSubListener(obj) {
    if (obj && typeof obj === 'object') {
      config.subs.push(obj)
    }
  },
  /*
     * 删除订阅事件
     * @params {obj} 订阅方对象
     */
  removeSubListener(obj) {
    if (obj && typeof obj === 'object') {
      config.subs = this.filter(v => v !== obj)
    }
  },
  /*
     * webSocket连接异常处理
     */
  onError() {
    this.reconnect()
  },
  /*
     * webSocket连接或者连接尝试关闭
     */
  close() {
    // debugger
    if (config.socketInstance) {
      config.socketInstance.close()
    }
  },
  /*
     * webSocket重连
     */
  reconnect() {
    this.close()
    if (config.interVal) {
      clearInterval(config.interVal)
    }
    config.socketInstance = null
    this.init()
  },
  /*
     * 侦听连接状态
     */
  checkConnectStatus(instance) {
    if (instance) {
      switch (instance.readyState) {
        case 0: {
          console.info('正在建立WebSocket连接中...')
          break
        }
        case 1: {
          console.info('WebSocket监听中...')
          const username = localStorage.getItem('username')
          if (!username) {
            config.url = ''
            this.close()
          }
          const data = {
            type: 'heartbeat',
            to: username,
            textType: 'String',
            text: 'Health check'
          }
          this.sendHeartbeat(JSON.stringify(data))
          config.serverTimeoutTask = setTimeout(() => {
            this.close()
          }, config.serverTimers)
          break
        }
        case 2: {
          console.warn('WebSocket连接正在进行关闭握手，即将关闭!')
          this.reconnect()
          break
        }
        case 3: {
          console.warn('WebSocket连接已经关闭或者根本没有建立, 重新连接中!')
          this.reconnect()
          break
        }
      }
    }
  }
}
