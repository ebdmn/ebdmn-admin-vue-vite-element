
import download from './download'
import websocket from './socket'

export default function installPlugins(app) {
  // 下载文件
  app.config.globalProperties.$download = download
  // websocket
  let socket = null
  if (app.config.globalProperties.$websocket) {
    socket = app.config.globalProperties.$websocket
  } else {
    socket = websocket
    Object.freeze(socket)
    app.config.globalProperties.$websocket = socket
  }
  socket.init()
}
