
/**
 * @description 配置建议在当前目录下修改config.js修改配置，会覆盖默认配置，也可以直接修改默认配置
 */
// 自定义配置

const globalSettings = {
  // 环境变量
  nodeEnv: 'development',
  // 开发以及部署时的URL，hash模式时在不确定二级目录名称的情况下建议使用""代表相对路径或者"/二级目录/"，history模式默认使用"/"或者"/二级目录/"
  publicPath: '',
  // 生产环境构建文件的目录名
  outputDir: 'dist',
  // 放置生成的静态资源 (js、css、img、fonts) 的 (相对于 outputDir 的) 目录。
  assetsDir: 'static',
  // 需要加loading层的请求，防止重复提交
  debounce: ['doEdit'],
  server: {
    // 环境端口号
    port: '8888',
    // 默认的接口地址
    baseURL:
        process.env.NODE_ENV === 'development' ? 'dev-api' : 'dev-api'
  },
  network: {
    // 配后端数据的接收方式application/json;charset=UTF-8 或 application/x-www-form-urlencoded;charset=UTF-8
    contentType: 'application/json;charset=UTF-8',
    // 消息框消失时间
    messageDuration: 3000,
    // 最长请求时间
    requestTimeout: 10000,
    // 操作正常code，支持String、Array、int多种类型
    successCode: [200, 0]
  },
  route: {
    // frontEndGeneration（前端生成路由）和 backendGeneration（后端导出路由）两种方式
    generationRouteBy: 'backendGeneration'
  },
  app: {
    // 标题 （包括初次加载雪花屏的标题 页面的标题 浏览器的标题）
    title: 'ebdmn-admin-vue-vite-element',
    // 标题是否反转 如果为false:"page - title"，如果为ture:"title - page"
    titleReverse: false,
    // 标题分隔符
    titleSeparator: ' - ',
    // 主题
    theme: 'default'
  },
  // 认证相关
  auth: {
    // token失效回退到登录页时是否记录本次的路由
    recordRoute: true,
    // 不经过token校验的路由
    routesWhiteList: ['/login', '/register', '/callback', '/404', '/403'],
    isNeedTokenInfo: true,
    // token名称
    tokenName: 'Authorization',
    // token在localStorage、sessionStorage、cookie存储的key的名称
    tokenTableName: 'accessToken'
  },
  // 底部版权
  copyright: {
    // 是否开启，同时在路由 meta 对象里可以单独设置某个路由是否显示底部版权信息
    enable: true,
    // 版权信息配置，格式为：Copyright © [dates] <company>, All Rights Reserved
    dates: '2020-2022',
    company: 'Ebdmn',
    website: 'https://gitee.com/ebdmn/ebdmn-admin-vue-vite-element.git'
  }
}
export default globalSettings
